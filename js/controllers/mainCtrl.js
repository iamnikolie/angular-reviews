
angular.module('mainCtrl', [])

    .controller('mainController', function($scope, $http, Item) {

        Item.get()
            .success(function(data) {
                $scope.items = data;
            });

        $scope.openSendModal = function() {
            $("#send-modal").modal('toggle');
        };

        $scope.clearReview = function() {
            $scope.reviewText = '';
        }

        $scope.sendReview = function() {
            var sended = {
                'id' : parseInt($scope.activeItem),
                'guid': guid(),
                'sessionid': sessionId,
                'fname': $scope.fname,
                'lname': $scope.lname,
                'timestamp': new Date(),
                'review': $scope.reviewText
            };
            console.log(sended);
            $("#send-modal").modal('toggle');

            Item.send('some/api/link', sended)
                .success(function(){
                    /*
                     * there is ***
                     */
                });

            /*
            * put this box to *** if your api link workin'
            */
            $scope.messageBoxText = "All your data sent!";
            $("#message-box").modal('toggle');
        }

    });