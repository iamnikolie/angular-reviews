
angular.module('itemService', [])
    .factory('Item', function($http) {
        return {
            get : function() {
                return $http.get('/angular-catalogue/data/test.json');
            },
            send : function (apilink, data) {
                return $http.put($.param(apilink), $.param(data));
            }

        }
    });